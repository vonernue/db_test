class CreateSProgramsSReviewers < ActiveRecord::Migration[7.0]
  def change
    create_table :s_programs_s_reviewers, id: false do |t|
      t.belongs_to :s_program, null: false, foreign_key: true
      t.belongs_to :s_reviewer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
