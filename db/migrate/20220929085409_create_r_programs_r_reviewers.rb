class CreateRProgramsRReviewers < ActiveRecord::Migration[7.0]
  def change
    create_table :r_programs_r_reviewers, id:false do |t|
      t.belongs_to :r_program, null: false, foreign_key: true
      t.belongs_to :r_reviewer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
