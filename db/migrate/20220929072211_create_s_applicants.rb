class CreateSApplicants < ActiveRecord::Migration[7.0]
  def change
    create_table :s_applicants do |t|
      t.string :username
      t.string :password
      t.boolean :isInit

      t.timestamps
    end
  end
end
