class CreateSApplicantsSPrograms < ActiveRecord::Migration[7.0]
  def change
    create_table :s_applicants_s_programs, id: false do |t|
      t.belongs_to :s_applicant, null: false, foreign_key: true
      t.belongs_to :s_programs, null: false, foreign_key: true
      t.integer :order
      t.string :file

      t.timestamps
    end
  end
end
