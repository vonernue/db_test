class CreateSGradings < ActiveRecord::Migration[7.0]
  def change
    create_table :s_gradings do |t|
      t.integer :docs_weight
      t.integer :oral_weight
      t.integer :docs_grade_name_1s
      t.integer :docs_grade_name_2
      t.integer :docs_grade_name_3
      t.integer :docs_grade_weight_1
      t.integer :docs_grade_weight_2
      t.integer :docs_grade_weight_3
      t.integer :oral_grade_name_1
      t.integer :oral_grade_name_2
      t.integer :oral_grade_name_3
      t.integer :oral_grade_weight_1
      t.integer :oral_grade_weight_2
      t.integer :oral_grade_weight_3
      t.belongs_to :s_program, null: false, foreign_key: true

      t.timestamps
    end
  end
end
