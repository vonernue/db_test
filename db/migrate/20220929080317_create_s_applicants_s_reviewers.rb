class CreateSApplicantsSReviewers < ActiveRecord::Migration[7.0]
  def change
    create_table :s_applicants_Ss_reviewers, id: false do |t|
      t.belongs_to :s_program, null: false, foreign_key: true
      t.belongs_to :s_applicant, null: false, foreign_key: true
      t.belongs_to :s_reviewer, null: false, foreign_key: true
      t.integer :docs_grade_1
      t.integer :docs_grade_2
      t.integer :docs_grade_3
      t.integer :oral_grade_1
      t.integer :oral_grade_2
      t.integer :oral_grade_3
      t.boolean :isDocsGraded
      t.boolean :isOralGraded

      t.timestamps
    end
  end
end
