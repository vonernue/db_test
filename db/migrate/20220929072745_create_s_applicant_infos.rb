class CreateSApplicantInfos < ActiveRecord::Migration[7.0]
  def change
    create_table :s_applicant_infos do |t|
      t.string :name
      t.string :email
      t.string :national_id
      t.string :admission_id
      t.string :sex
      t.date :birth
      t.string :day_phone
      t.string :night_phone
      t.string :mobile_phone
      t.string :hosuehold_address
      t.string :household_zipcode
      t.string :communicate_address
      t.string :communicate_zipcode
      t.string :graduated_school
      t.string :graduated_major
      t.boolean :isSameDept

      t.belongs_to :s_Applicant, null: false, foreign_key: true

      t.timestamps
    end
  end
end
