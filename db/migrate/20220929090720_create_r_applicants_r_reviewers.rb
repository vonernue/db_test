class CreateRApplicantsRReviewers < ActiveRecord::Migration[7.0]
  def change
    create_table :r_applicants_r_reviewers do |t|
      t.belongs_to :r_applicant, null: false, foreign_key: true
      t.belongs_to :r_reviewer, null: false, foreign_key: true
      t.belongs_to :r_program, null: false, foreign_key: true
      t.string :comment
      t.boolean :isRecommend
      t.boolean :isSeen

      t.timestamps
    end
  end
end
