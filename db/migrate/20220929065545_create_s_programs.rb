class CreateSPrograms < ActiveRecord::Migration[7.0]
  def change
    create_table :s_programs do |t|
      t.string :category
      t.string :name
      t.date :start_date
      t.date :end_date
      t.string :require_file

      t.timestamps
    end
  end
end
