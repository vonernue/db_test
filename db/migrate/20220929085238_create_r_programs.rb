class CreateRPrograms < ActiveRecord::Migration[7.0]
  def change
    create_table :r_programs do |t|
      t.string :category
      t.string :name
      t.date :start_date
      t.date :end_date
      t.string :requires_files

      t.timestamps
    end
  end
end
