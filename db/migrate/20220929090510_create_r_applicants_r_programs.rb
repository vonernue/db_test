class CreateRApplicantsRPrograms < ActiveRecord::Migration[7.0]
  def change
    create_table :r_applicants_r_programs, id:false do |t|
      t.belongs_to :r_applicant, null: false, foreign_key: true
      t.belongs_to :r_program, null: false, foreign_key: true
      t.integer :order
      t.string :files

      t.timestamps
    end
  end
end
