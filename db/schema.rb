# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_09_29_090720) do
  create_table "admins", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_applicant_infos", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "national_id"
    t.string "sex"
    t.date "birth"
    t.string "day_phone"
    t.string "night_phone"
    t.string "mobile_phone"
    t.string "hosuehold_address"
    t.string "household_zipcode"
    t.string "communicate_address"
    t.string "communicate_zipcode"
    t.string "graduated_school"
    t.string "graduated_major"
    t.boolean "isSameDept"
    t.integer "r_applicant_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["r_applicant_id"], name: "index_r_applicant_infos_on_r_applicant_id"
  end

  create_table "r_applicants", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_applicants_r_programs", id: false, force: :cascade do |t|
    t.integer "r_applicant_id", null: false
    t.integer "r_program_id", null: false
    t.integer "order"
    t.string "files"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["r_applicant_id"], name: "index_r_applicants_r_programs_on_r_applicant_id"
    t.index ["r_program_id"], name: "index_r_applicants_r_programs_on_r_program_id"
  end

  create_table "r_applicants_r_reviewers", force: :cascade do |t|
    t.integer "r_applicant_id", null: false
    t.integer "r_reviewer_id", null: false
    t.integer "r_program_id", null: false
    t.string "comment"
    t.boolean "isRecommend"
    t.boolean "isSeen"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["r_applicant_id"], name: "index_r_applicants_r_reviewers_on_r_applicant_id"
    t.index ["r_program_id"], name: "index_r_applicants_r_reviewers_on_r_program_id"
    t.index ["r_reviewer_id"], name: "index_r_applicants_r_reviewers_on_r_reviewer_id"
  end

  create_table "r_managers", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_programs", force: :cascade do |t|
    t.string "category"
    t.string "name"
    t.date "start_date"
    t.date "end_date"
    t.string "requires_files"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "r_programs_r_reviewers", id: false, force: :cascade do |t|
    t.integer "r_program_id", null: false
    t.integer "r_reviewer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["r_program_id"], name: "index_r_programs_r_reviewers_on_r_program_id"
    t.index ["r_reviewer_id"], name: "index_r_programs_r_reviewers_on_r_reviewer_id"
  end

  create_table "r_reviewers", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "s_applicant_infos", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "national_id"
    t.string "admission_id"
    t.string "sex"
    t.date "birth"
    t.string "day_phone"
    t.string "night_phone"
    t.string "mobile_phone"
    t.string "hosuehold_address"
    t.string "household_zipcode"
    t.string "communicate_address"
    t.string "communicate_zipcode"
    t.string "graduated_school"
    t.string "graduated_major"
    t.boolean "isSameDept"
    t.integer "s_Applicant_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["s_Applicant_id"], name: "index_s_applicant_infos_on_s_Applicant_id"
  end

  create_table "s_applicants", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.boolean "isInit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "s_applicants_Ss_reviewers", id: false, force: :cascade do |t|
    t.integer "s_program_id", null: false
    t.integer "s_applicant_id", null: false
    t.integer "s_reviewer_id", null: false
    t.integer "docs_grade_1"
    t.integer "docs_grade_2"
    t.integer "docs_grade_3"
    t.integer "oral_grade_1"
    t.integer "oral_grade_2"
    t.integer "oral_grade_3"
    t.boolean "isDocsGraded"
    t.boolean "isOralGraded"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["s_applicant_id"], name: "index_s_applicants_Ss_reviewers_on_s_applicant_id"
    t.index ["s_program_id"], name: "index_s_applicants_Ss_reviewers_on_s_program_id"
    t.index ["s_reviewer_id"], name: "index_s_applicants_Ss_reviewers_on_s_reviewer_id"
  end

  create_table "s_applicants_s_programs", id: false, force: :cascade do |t|
    t.integer "s_applicant_id", null: false
    t.integer "s_programs_id", null: false
    t.integer "order"
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["s_applicant_id"], name: "index_s_applicants_s_programs_on_s_applicant_id"
    t.index ["s_programs_id"], name: "index_s_applicants_s_programs_on_s_programs_id"
  end

  create_table "s_gradings", force: :cascade do |t|
    t.integer "docs_weight"
    t.integer "oral_weight"
    t.integer "docs_grade_name_1s"
    t.integer "docs_grade_name_2"
    t.integer "docs_grade_name_3"
    t.integer "docs_grade_weight_1"
    t.integer "docs_grade_weight_2"
    t.integer "docs_grade_weight_3"
    t.integer "oral_grade_name_1"
    t.integer "oral_grade_name_2"
    t.integer "oral_grade_name_3"
    t.integer "oral_grade_weight_1"
    t.integer "oral_grade_weight_2"
    t.integer "oral_grade_weight_3"
    t.integer "s_program_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["s_program_id"], name: "index_s_gradings_on_s_program_id"
  end

  create_table "s_managers", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "s_programs", force: :cascade do |t|
    t.string "category"
    t.string "name"
    t.date "start_date"
    t.date "end_date"
    t.string "require_file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "s_programs_s_reviewers", id: false, force: :cascade do |t|
    t.integer "s_program_id", null: false
    t.integer "s_reviewer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["s_program_id"], name: "index_s_programs_s_reviewers_on_s_program_id"
    t.index ["s_reviewer_id"], name: "index_s_programs_s_reviewers_on_s_reviewer_id"
  end

  create_table "s_reviewers", force: :cascade do |t|
    t.string "username"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "r_applicant_infos", "r_applicants"
  add_foreign_key "r_applicants_r_programs", "r_applicants"
  add_foreign_key "r_applicants_r_programs", "r_programs"
  add_foreign_key "r_applicants_r_reviewers", "r_applicants"
  add_foreign_key "r_applicants_r_reviewers", "r_programs"
  add_foreign_key "r_applicants_r_reviewers", "r_reviewers"
  add_foreign_key "r_programs_r_reviewers", "r_programs"
  add_foreign_key "r_programs_r_reviewers", "r_reviewers"
  add_foreign_key "s_applicant_infos", "s_Applicants"
  add_foreign_key "s_applicants_Ss_reviewers", "s_applicants"
  add_foreign_key "s_applicants_Ss_reviewers", "s_programs"
  add_foreign_key "s_applicants_Ss_reviewers", "s_reviewers"
  add_foreign_key "s_applicants_s_programs", "s_applicants"
  add_foreign_key "s_applicants_s_programs", "s_programs", column: "s_programs_id"
  add_foreign_key "s_gradings", "s_programs"
  add_foreign_key "s_programs_s_reviewers", "s_programs"
  add_foreign_key "s_programs_s_reviewers", "s_reviewers"
end
