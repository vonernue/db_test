class SProgram < ApplicationRecord
    has_one :S_Gradings
    has_and_belongs_to_many :S_Reviewers
    has_many :S_Applicants, through: :S_Applicants_S_Programs
end
