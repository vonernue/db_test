class SReviewer < ApplicationRecord
    has_and_belongs_to_many :S_Programs
    has_many :S_Applicants, through: :S_Applicants_S_Reviewers
end
