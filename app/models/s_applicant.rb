class SApplicant < ApplicationRecord
    has_one :S_Applicant_Info
    has_many :S_Reviewers, through: :S_Applicants_S_Reviewers
    has_many :S_Programs, through: :S_Applicants_S_Programs
end
