class RApplicantsRReviewers < ApplicationRecord
  belongs_to :r_applicant
  belongs_to :r_reviewer
  belongs_to :r_program
end
