class SApplicantsSReviewers < ApplicationRecord
  belongs_to :S_Program
  belongs_to :S_Applicant
  belongs_to :S_Reviewer
end
