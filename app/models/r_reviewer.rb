class RReviewer < ApplicationRecord
    has_many :r_applicants, through: :r_applicants_r_reviewers
    has_only_belongs_to_many :r_programs
end
