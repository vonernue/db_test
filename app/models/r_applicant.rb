class RApplicant < ApplicationRecord
    has_one :r_applicant_info
    has_many :r_programs, through: :r_applicants_r_programs
    has_many :r_reviewers, through: :r_applicants_r_reviewers
end
